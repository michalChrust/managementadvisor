<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.bluesoft.managementadvisor.database.sessioncreator.DatabaseQueryHandler" %>
<%@ page import="com.bluesoft.managementadvisor.database.entities.SystemEntity" %>
<%@ page import="com.bluesoft.managementadvisor.database.entities.ContractEntity" %>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="bluesoft.css" type="text/css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>

        $(document).ready( function () {
            // creating app elements
            $(".widget button").button();

            var activeContracts = $('#myTable-1').DataTable();

            var allContracts = $('#myTable-2').DataTable();

            var systems = $('#myTable-3').DataTable();

            $( "#accordion" ).accordion();

            $( ".dialog" ).dialog({
                dialogClass: "no-close",
                resizable: false,
                position: { my: "bottom+100px", at: "center", of: window },
                modal:true,
                draggable:false,
                minWidth:350,
                autoOpen:false,
                dialogClass: 'noTitleStuff'
            });


            $( ".datepicker" ).datepicker();

            $(".selectmenu").selectmenu({
                width:200,
            });
                $( "#tabs" ).tabs();
                $( "#addTab" ).tabs();
                $( "#editTab" ).tabs();
            $( "#deleteTab" ).tabs();

            // setting events
            $("#addButton").click(function(){
                $("#addDialog").dialog("open");
            });

            $("#editButton").click(function(){
                $("#editDialog").dialog("open");
            });

            $("#deleteButton").click(function(){
                $("#deleteDialog").dialog("open");
            });

            $("#addDialog").dialog("option","buttons",
                [
                    {
                        text: "Akceptuj",
                        click: function() {
                            activeContracts.row.add([
                                document.getElementById("contractNameAdd").value,
                                document.getElementById("systemNameInContractAdd").value,
                                document.getElementById("contractFromDatePickerAdd").value,
                                document.getElementById("contractToDatePickerAdd").value,
                                document.getElementById("contractAmountAdd").value,
                                document.getElementById("selectmenuPayoffAdd").value,
                                document.getElementById("selectmenuIfActiveAdd").value
                            ]).draw(true);

                            document.getElementById("contractNameAdd").value = null,
                                document.getElementById("systemNameInContractAdd").value = null,
                                document.getElementById("contractFromDatePickerAdd").value = null,
                                document.getElementById("contractToDatePickerAdd").value = null,
                                document.getElementById("contractAmountAdd").value = null,
                                document.getElementById("selectmenuPayoffAdd").value = null,
                                document.getElementById("selectmenuIfActiveAdd").value = null
                            $(this).dialog("close");

                            <%
                            ContractEntity  contractEntity = new ContractEntity(Integer.valueOf(request.getParameter("C1")),
                             request.getParameter("C2"),
                             request.getParameter("C3"),
                             request.getParameter("C4"),
                             Double.valueOf(request.getParameter("C5")),
                             request.getParameter("C6"),
                             request.getParameter("C7"));
                            DatabaseQueryHandler databaseQueryHandler = new DatabaseQueryHandler();
                            databaseQueryHandler.saveContract(contractEntity);
                            %>
                        }
                    },
                    {
                        text: "Powrót",
                        click: function(){
                            $(this).dialog("close");
                        }
                    }
                ]);

            $("#editDialog").dialog("option","buttons",
                [
                    {
                        text: "Akceptuj",
                        click: function() {




                            $(this).dialog("close");
                        }
                    },
                    {
                        text: "Powrót",
                        click: function(){
                            $(this).dialog("close");
                        }
                    }
                ]);

            $("#deleteDialog").dialog("option","buttons",
                [
                    {
                        text: "Akceptuj",
                        click: function() {
                            $(this).dialog("close");
                        }
                    },
                    {
                        text: "Powrót",
                        click: function(){
                            $(this).dialog("close");
                        }
                    }
                ]);
        });

    </script>
    <title>Menagement Advisor</title>
</head>

<!-- BODY -->
<body>
<!-- CRUD buttons -->
<ul class="widget" style="margin-left:0px; margin-top:0px">
    <button id="addButton">Dodaj</button>
    <button id="editButton">Edytuj</button>
    <button id="deleteButton">Usuń</button>
</ul>
<!-- CRUD dialogs -->
<div class="dialog" id="addDialog">
    <div id="addTab">
        <ul>
            <li><a href="#addTab-1">Umowę</a></li>
            <li><a href="#addTab-2">System</a></li>
        </ul>

        <div id="addTab-1">
            <ul style="list-style-type:none">
                <label for="contractNameAdd">Numer umowy</label>
                <li><form method="get">
                    <input id="contractNameAdd" type="text" name="C1">
                </form>
                    </li>
                <label for="systemNameInContractAdd">System</label>
                <li>
                    <form method="get">
                        <input id="systemNameInContractAdd" type="text" name="C3">
                    </form>
                    </li>
                <label for="contractFromDatePickerAdd">Od</label>
                <li><form method="get">
                    <input class="datepicker" id="contractFromDatePickerAdd" type="text" name="C3">
                </form></li>
                <label for="contractToDatePickerAdd">Do</label>
                <li><form method="get">
                    <input class="datepicker" id="contractToDatePickerAdd" type="text" name="C4">
                </form></li>
                <label for="contractAmountAdd">Kwota umowy</label>
                <li><form method="get">
                    <input id="contractAmountAdd" name="C5">
                </form></li>
                <label for="selectmenuPayoffAdd">Rozliczenie</label>
                <li><form method="get">
                    <select class="selectmenu" id="selectmenuPayoffAdd" name="C6">
                        <option>Tygodniowe</option>
                        <option>Miesięczne</option>
                        <option>Kwartalne</option>
                        <option>Roczne</option>
                    </select>
                </form></li>
                <label for="selectmenuIfActiveAdd">Czy umowa jest aktywna</label>
                <li><form method="get">
                    <select class="selectmenu" id="selectmenuIfActiveAdd" name="C7">
                        <option>Tak</option>
                        <option>Nie</option>
                    </select>
                </form> </li>
            </ul>
        </div>

        <div id="addTab-2">
            <ul style="list-style-type:none">
                <label for="systemNameAdd">Nazwa systemu</label>
                <li><input id="systemNameAdd" type="text"><br><br></li>
                <label for="systemDescriptionAdd">Krótki opis systemu</label>
                <li><input id="systemDescriptionAdd" type="text"><br><br></li>
                <label for="technologiesAdd">Krótki opis wiodących technologii</label>
                <li><input  id="technologiesAdd" type="text"><br><br></li>
                <label for="systemOwnerAdd">Właściciel systemu</label>
                <li><input id="systemOwnerAdd" type="text"><br><br></li>
            </ul>
        </div>
    </div>
</div>
<div class="dialog" id="editDialog">
    <div id="editTab">
        <ul>
            <li><a href="#editTab-1">Umowę</a></li>
            <li><a href="#editTab-2">System</a></li>
        </ul>

        <div id="editTab-1">
            <ul style="list-style-type:none">
                <label for="contractNameEdit">Numer umowy do edycji</label>
                <li><input id="contractNameEdit" type="text"><br><br></li>
                <label>Nowe dane umowy:</label>
                <hr><br>
                <label for="systemNameInContractEdit">System</label>
                <li><input id="systemNameInContractEdit" type="text"><br><br></li>
                <label for="contractFromDatePickerEdit">Od</label>
                <li><input class="datepicker" id="contractFromDatePickerEdit" type="text"><br><br></li>
                <label for="contractToDatePickerEdit">Do</label>
                <li><input class="datepicker" id="contractToDatePickerEdit" type="text"><br><br></li>
                <label for="contractAmountEdit">Kwota umowy</label>
                <li><input id="contractAmountEdit"><br><br></li>
                <label for="selectmenuPayoffEdit">Rozliczenie</label>
                <li><select class="selectmenu" id="selectmenuPayoffEdit">
                    <option>Tygodniowe</option>
                    <option>Miesięczne</option>
                    <option>Kwartalne</option>
                    <option>Roczne</option>
                </select><br><br></li>
                <label for="selectmenuIfActiveEdit">Czy umowa jest aktywna</label>
                <li><select class="selectmenu" id="selectmenuIfActiveEdit">
                    <option>Tak</option>
                    <option>Nie</option>
                </select></li></ul>
        </div>

        <div id="editTab-2">
            <ul style="list-style-type:none">
                <label for="systemNameEdit">Nazwa systemu do edycji</label>
                <li><input id="systemNameEdit" type="text"><br><br></li>
                <label>Nowe dane systemu:</label>
                <hr><br>
                <label for="systemEditDescriptionEdit">Krótki opis systemu</label>
                <li><input id="systemEditDescriptionEdit" type="text"><br><br></li>
                <label for="technologiesEdit">Krótki opis wiodących technologii</label>
                <li><input  id="technologiesEdit" type="text"><br><br></li>
                <label for="systemOwnerEdit">Właściciel systemu</label>
                <li><input id="systemOwnerEdit" type="text"><br><br></li>
            </ul>
        </div>
    </div>
</div>
<div class="dialog" id="deleteDialog">
    <div id="deleteTab">
        <ul>
            <li><a href="#deleteTab-1">Umowę</a></li>
            <li><a href="#deleteTab-2">System</a></li>
        </ul>

        <div id="deleteTab-1">
            <ul style="list-style-type:none">
                <label for="deleteContractNumber">Numer umowy do usunięcia</label>
                <li><input id="deleteContractNumber" type="text"></li></ul>
        </div>

        <div id="deleteTab-2">
            <ul style="list-style-type:none">
                <label for="deleteSystemName">Nazwa systemu do usunięcia</label>
                <li><input id="deleteSystemName" type="text"></li></ul>
        </div>
    </div>
</div>
<!-- preview tables -->
<div id="tabs">
    <ul>
        <li><a href="#tabs-1">Aktywne umowy</a></li>
        <li><a href="#tabs-2">Wszystkie umowy</a></li>
        <li><a href="#tabs-3">Systemy</a></li>
        <li><a href="#tabs-4">O Aplikacji</a></li>
    </ul>
    <!-- 1st tab -->
    <div id="tabs-1">
        <div class="unit w-2-3">
            <div class="hero-callout">
                <table id="myTable-1" class="display" style="width:100%">
                    <thead>
                    <!-- tables head -->
                    <tr>
                        <th>Numer Umowy</th>
                        <th>System</th>
                        <th>Od</th>
                        <th>Do</th>
                        <th>Kwota umowy</th>
                        <th>Informacja o rozliczeniu</th>
                        <th>Czy umowa jest aktywna</th>
                    </tr>
                    </thead>
                    <!-- datas  -->
                    <tbody>
                    <c:forEach var="contract" items="${activeContractList}">
                        <tr>
                            <td>${contract.contractNumber}</td>
                            <td>${contract.systemName}</td>
                            <td>${contract.fromDate}</td>
                            <td>${contract.toDate}</td>
                            <td>${contract.contractAmount}</td>
                            <td>${contract.payoffInformation}</td>
                            <td>${contract.ifContractIsActive}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                    <!-- foot element -->
                    <tfoot>
                    <tr>
                        <th>Numer Umowy</th>
                        <th>System</th>
                        <th>Od</th>
                        <th>Do</th>
                        <th>Kwota umowy</th>
                        <th>Informacja o rozliczeniu</th>
                        <th>Czy umowa jest aktywna</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <!-- 2nd tab -->
    <div id="tabs-2">
        <div class="unit w-2-3">
            <div class="hero-callout">
                <table id="myTable-2" class="display" style="width:100%">
                    <thead>
                    <!-- tables head -->
                    <tr>
                        <th>Numer Umowy</th>
                        <th>System</th>
                        <th>Od</th>
                        <th>Do</th>
                        <th>Kwota umowy</th>
                        <th>Informacja o rozliczeniu</th>
                        <th>Czy umowa jest aktywna</th>
                    </tr>
                    </thead>
                    <!-- datas  -->
                    <tbody>
                    <c:forEach var="contract" items="${allContractList}">
                        <tr>
                            <td>${contract.contractNumber}</td>
                            <td>${contract.systemName}</td>
                            <td>${contract.fromDate}</td>
                            <td>${contract.toDate}</td>
                            <td>${contract.contractAmount}</td>
                            <td>${contract.payoffInformation}</td>
                            <td>${contract.ifContractIsActive}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                    <!-- foot element -->
                    <tfoot>
                    <tr>
                        <th>Numer Umowy</th>
                        <th>System</th>
                        <th>Od</th>
                        <th>Do</th>
                        <th>Kwota umowy</th>
                        <th>Informacja o rozliczeniu</th>
                        <th>Czy umowa jest aktywna</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <!-- 3rd table -->
    <div id="tabs-3">
        <div class="unit w-2-3">
            <div class="hero-callout">
                <table id="myTable-3" class="display" style="width:100%">
                    <thead>
                    <!-- tables head -->
                    <tr>
                        <th>Nazwa systemu</th>
                        <th>Krótki opis systemu</th>
                        <th>Krótki opis wiodących technologii</th>
                        <th>Właściciel systemu</th>
                    </tr>
                    </thead>
                    <!-- datas  -->
                    <tbody>
                    <c:forEach var="system" items="${systemList}">
                        <tr>
                            <td>${system.systemName}</td>
                            <td>${system.systemDescription}</td>
                            <td>${system.technologies}</td>
                            <td>${system.systemOwner}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                    <!-- foot element -->
                    <tfoot>
                    <tr>
                        <th>Nazwa systemu</th>
                        <th>Krótki opis systemu</th>
                        <th>Krótki opis wiodących technologii</th>
                        <th>Właściciel systemu</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <!-- 4th tab -->
    <div id="tabs-4">
        <div id="accordion">
            <h3>Autor</h3>
            <div>
                <p>
                    Autorem tej aplikacji jest Michał Chrust.<br>
                    Dane kontaktowe:<br>
                    tel: 729-410-473<br>
                    e-mail:michal.chrust@outlook.com<br>
                    Michał jest osobą posiadającą wymagające gusta smakowe. Nie toleruje rodzynek w serniku i uwielbia pizzę hawajską.
                </p>
            </div>
            <h3>Data wykonania zadania</h3>
            <div>
                <p>
                    Data wykonania wersji aplikacji wysłanej do klienta to 22.11.2019.
                </p>
            </div>
            <h3>Pozdrowienia</h3>
            <div>
                <p>
                    Pozdrawiamy wszystkich czytelników tekstu, niech moc będzie z wami! :3
                </p>
            </div>
        </div>
    </div>
</div>
</body>
</html>
