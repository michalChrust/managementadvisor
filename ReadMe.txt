Podsumowanie:
Aplikacja posiada podstawowe funkcjonalności, łączy się z bazą danych(niestety myql) oraz wyświetla zapisane tam rekordy.
Do aplikacji zostały dodane funkcjonalności związane z zapisywaniem i odczytem danych z bazy, niestety zapis nie został zaimplementowany w GUI (backend nie
został połączony z front-endem).
W największym stopniu (w mojej opini) mogę być dumny z w pełni gotowej warstwy wizualnej aplikacji.
Nie została w niej wykorzystana biblioteka Apache Tiles (zanim przygotowałem front nie sprawdziłem do czego służy wymieniona biblioteka. Byłem pewien,
że spełnia funkcjonalności związane z mechaniką aplikacji jak Apache Poi, myliłem się).
Dziękuję za możliwość aplikacji, niezależnie od wyniku rekrutacji mogę zdecydowanie przyznać, że wykonanie zadania pozwoliło mi zrobić wielki krok naprzód
w moim rozwoju jako programista.
Pozdrawiam,
Michał Chrust