package com.bluesoft.managementadvisor.controllers;

import com.bluesoft.managementadvisor.database.entities.ContractEntity;
import com.bluesoft.managementadvisor.database.entities.SystemEntity;
import com.bluesoft.managementadvisor.database.sessioncreator.DatabaseQueryHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * This class realize the main mechanics of application and open the html (+css and js) code.
 * @author Michał Chrust
 * @version 0.0.1
 */
@Controller
public class MainController {
    private static final Logger logger = LoggerFactory.getLogger(MainController.class);

    @RequestMapping("/")
    public String openHomePage(Model model){
        logger.info("Application started.");
        logger.info("Loading data to tables");
        //creating instance of database handler
        DatabaseQueryHandler databaseQueryHandler = new DatabaseQueryHandler();
        //creating lists of items to fill datatable
        List<ContractEntity> activeContractEntityList = databaseQueryHandler.loadActiveContracts();
        List<ContractEntity> allContractEntityList = databaseQueryHandler.loadAllContracts();
        List<SystemEntity> systemEntityList = databaseQueryHandler.loadSystems();
        //adding lists to model during loading
        model.addAttribute("activeContractList" , activeContractEntityList);
        model.addAttribute("allContractList" , allContractEntityList);
        model.addAttribute("systemList" , systemEntityList);
        //adding empty entities to model to get way to save it in database
        ContractEntity contractEntity = new ContractEntity();
        SystemEntity systemEntity = new SystemEntity();
        model.addAttribute(contractEntity);
        model.addAttribute(systemEntity);
        return "index";
    }
}
