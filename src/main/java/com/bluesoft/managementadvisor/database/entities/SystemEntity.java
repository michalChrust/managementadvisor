package com.bluesoft.managementadvisor.database.entities;

import javax.persistence.*;

/**
 * This entity class contains only contract fields, no-arg-constructor, arg-contructor, getters, setters and overriden to-String method
 * @author Michał Chrust
 * @version 0.0.1
 */
@Entity
@Table(name = "systems")
public class SystemEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "system_id")
    private int systemId;

    @Column(name = "system_name")
    private String systemName;

    @Column(name = "system_description")
    private String systemDescription;

    @Column(name = "technologies")
    private String technologies;

    @Column(name = "system_owner")
    private String systemOwner;

    public SystemEntity() {
    }

    public SystemEntity(String systemName, String systemDescription, String technologies, String systemOwner) {
        this.systemName = systemName;
        this.systemDescription = systemDescription;
        this.technologies = technologies;
        this.systemOwner = systemOwner;
    }

    public int getSystemId() {
        return systemId;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public String getSystemDescription() {
        return systemDescription;
    }

    public void setSystemDescription(String systemDescription) {
        this.systemDescription = systemDescription;
    }

    public String getTechnologies() {
        return technologies;
    }

    public void setTechnologies(String technologies) {
        this.technologies = technologies;
    }

    public String getSystemOwner() {
        return systemOwner;
    }

    public void setSystemOwner(String systemOwner) {
        this.systemOwner = systemOwner;
    }

    @Override
    public String toString() {
        return "SystemEntity{" +
                "systemId=" + systemId +
                ", systemName='" + systemName + '\'' +
                ", systemDescription='" + systemDescription + '\'' +
                ", technologies='" + technologies + '\'' +
                ", systemOwner='" + systemOwner + '\'' +
                '}';
    }
}
