package com.bluesoft.managementadvisor.database.entities;

import javax.persistence.*;

/**
 * This entity class contains only contract fields, no-arg-constructor, arg-contructor, getters, setters and overriden to-String method
 * @author Michał Chrust
 * @version 0.0.1
 */
@Entity
@Table(name = "contracts")
public class ContractEntity {

    @Id
    @Column(name = "contract_number")
    private int contractNumber;

    @Column(name = "system_name")
    private String systemName;

    @Column(name = "from_date")
    private String fromDate;

    @Column(name = "to_date")
    private String toDate;

    @Column(name = "contract_amount")
    private double contractAmount;

    @Column(name = "payoff_information")
    private String payoffInformation;

    @Column(name = "if_contract_is_active")
    private String ifContractIsActive;

    public ContractEntity() {
    }

    public ContractEntity(int contractNumber, String systemName, String fromDate, String toDate, double contractAmount, String payoffInformation, String ifContractIsActive) {
        this.contractNumber = contractNumber;
        this.systemName = systemName;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.contractAmount = contractAmount;
        this.payoffInformation = payoffInformation;
        this.ifContractIsActive = ifContractIsActive;
    }

    public int getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(int contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public double getContractAmount() {
        return contractAmount;
    }

    public void setContractAmount(double contractAmount) {
        this.contractAmount = contractAmount;
    }

    public String getPayoffInformation() {
        return payoffInformation;
    }

    public void setPayoffInformation(String payoffInformation) {
        this.payoffInformation = payoffInformation;
    }

    public String getIfContractIsActive() {
        return ifContractIsActive;
    }

    public void setIfContractIsActive(String ifContractIsActive) {
        this.ifContractIsActive = ifContractIsActive;
    }

    @Override
    public String toString() {
        return "ContractEntity{" +
                "contractNumber='" + contractNumber + '\'' +
                ", systemName='" + systemName + '\'' +
                ", fromDate='" + fromDate + '\'' +
                ", toDate='" + toDate + '\'' +
                ", contractAmount='" + contractAmount + '\'' +
                ", payoffInformation='" + payoffInformation + '\'' +
                ", ifContractIsActive='" + ifContractIsActive + '\'' +
                '}';
    }
}
