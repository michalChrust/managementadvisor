package com.bluesoft.managementadvisor.database.sessioncreator;

import com.bluesoft.managementadvisor.database.entities.ContractEntity;
import com.bluesoft.managementadvisor.database.entities.SystemEntity;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * This class provide all necessary function to handle crud operations on connected database.
 * @author: Michał Chrust
 * @version 0.0.1
 */
public class DatabaseQueryHandler {
    //FIELDS

    private static final Logger logger = LoggerFactory.getLogger(DatabaseQueryHandler.class);

    private SessionCreator sessionCreator = new SessionCreator();

    //METHODS
    //load methods
    public List<ContractEntity> loadActiveContracts(){
        Session session = sessionCreator.getContractSession();
        session.beginTransaction();
        logger.info("Transaction for loading active contracts has been begun");
        List<ContractEntity> contractEntityList = session.createQuery("from ContractEntity c where c.ifContractIsActive = 'tak'").getResultList();
        logger.info(" Active contract list has been received");
        session.getTransaction().commit();
        sessionCreator.closeContractSessionFactory();
        return contractEntityList;
    }

    public List<ContractEntity> loadAllContracts(){
        Session session = sessionCreator.getContractSession();
        session.beginTransaction();
        logger.info("Transaction for loading all contracts has been begun");
        List<ContractEntity> contractEntityList = session.createQuery("from ContractEntity").getResultList();
        logger.info("All contract list has been received");
        session.getTransaction().commit();
        sessionCreator.closeContractSessionFactory();
        return contractEntityList;
    }

    public List<SystemEntity> loadSystems(){
        Session session = sessionCreator.getSystemSession();
        session.beginTransaction();
        logger.info("Transaction for loading systems has been begun");
        List<SystemEntity> systemEntityList = session.createQuery("from SystemEntity").getResultList();
        logger.info("System list has been received");
        session.getTransaction().commit();
        sessionCreator.closeSystemSessionFactory();
        return systemEntityList;
    }
    //save methods
    public void saveContract(ContractEntity contractEntity){
        Session session = sessionCreator.getContractSession();
        session.beginTransaction();
        logger.info("Transaction for saving contract has been begun");
        session.save(contractEntity);
        logger.info("Data has been saved");
        session.getTransaction().commit();
        sessionCreator.closeContractSessionFactory();
    }

    public void saveSystem(SystemEntity systemEntity){
        Session session = sessionCreator.getSystemSession();
        session.beginTransaction();
        logger.info("Transaction for saving system has been begun");
        session.save(systemEntity);
        logger.info("Data has been saved");
        session.getTransaction().commit();
        sessionCreator.closeSystemSessionFactory();
    }
}
