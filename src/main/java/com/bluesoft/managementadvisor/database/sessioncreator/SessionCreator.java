package com.bluesoft.managementadvisor.database.sessioncreator;

import com.bluesoft.managementadvisor.database.entities.ContractEntity;
import com.bluesoft.managementadvisor.database.entities.SystemEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class provide all necessary function to set a connection with database database.
 * @author: Michał Chrust
 * @version 0.0.1
 */
public class SessionCreator {
    private static final Logger logger = LoggerFactory.getLogger(SessionCreator.class);

    public SessionFactory getContractSessionFactory(){
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(ContractEntity.class)
                .buildSessionFactory();
        logger.info("SessionFactory set for contracts");
        return factory;
    }

    public Session getContractSession(){
        Session session = getContractSessionFactory().getCurrentSession();
        logger.info("Session got for contracts");
        return session;
    }

    public void closeContractSessionFactory(){
        getContractSessionFactory().close();
        logger.info("SessionFactory for contracts closed");
    }

    public SessionFactory getSystemSessionFactory(){
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(SystemEntity.class)
                .buildSessionFactory();
        logger.info("SessionFactory set for systems");
        return factory;
    }

    public Session getSystemSession() {
        Session session = getSystemSessionFactory().getCurrentSession();
        logger.info("Session got for systems");
        return session;
    }

    public void closeSystemSessionFactory(){
        getSystemSessionFactory().close();
        logger.info("SessionFactory for contracts closed");
    }
}
